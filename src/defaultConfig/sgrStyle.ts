const sgrStyle = {
    "black": "rgb(0, 0, 0)",
    "red": "rgb(170, 0, 0)",
    "green": "rgb(0, 170, 0)",
    "yellow": "rgb(170, 85, 0)",
    "blue": "rgb(0, 0, 170)",
    "magenta": "rgb(170, 0, 170)",
    "cyan": "rgb(0, 170, 170)",
    "white": "rgb(170, 170, 170)",
    "brightBlack": "rgb(85, 85, 85)",
    "brightRed": "rgb(255, 85, 85)",
    "brightGreen": "rgb(85, 255, 85)",
    "brightYellow": "rgb(255, 255, 85)",
    "brightBlue": "rgb(85, 85, 255)",
    "brightMagenta": "rgb(255, 85, 255)",
    "brightCyan": "rgb(85, 255, 255)",
    "brightWhite": "rgb(255, 255, 255)"
};

export default sgrStyle;