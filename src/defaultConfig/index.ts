// @flow

import hotkeyMapping from './hotkeyMapping.json';
import keyMacros from './keyMacros.json';
import sgrStyle from './sgrStyle';

export {
    hotkeyMapping,
    sgrStyle,
    keyMacros
}