import {action, observable} from "mobx";

export class AppState {
    terminalSettings: {
        minHistoryLength: number,
        minDisplayLength: number,
        echoStyle: number[]
    };
    errorMessages: Array<string>;
    configuratorDisplayed: any;
    settingsPopupDisplayed: any;
    specialPageTitle: any;
    blinkingTitleEnabled: any;

    constructor() {
        this.terminalSettings = {
            minHistoryLength: 50,
            minDisplayLength: 500,
            echoStyle: [32] // default value is dark green
        };
        this.errorMessages = observable([]);
        this.configuratorDisplayed = observable.box(false);
        this.settingsPopupDisplayed = observable.box(false);
        this.specialPageTitle = observable.box(undefined);
        this.blinkingTitleEnabled = observable.box(true);

        this.loadPersistentSettingsFromLocalStorage();
    }

    addErrorMessage = action((message: string) => {
        this.errorMessages.push(message);
    });
    closeErrorMessage = action((index: string) => {
        const numericIndex = parseInt(index);
        if (numericIndex >= 0 && numericIndex < this.errorMessages.length) {
            this.errorMessages.splice(numericIndex, 1);
        }
    });
    displayHotkeyConfigurator = action(() => {
        this.settingsPopupDisplayed.set(false);
        this.configuratorDisplayed.set(true);
    });
    hideHotkeyConfigurator = action(() => {
        this.configuratorDisplayed.set(false);
    });
    displaySettingsPopup = action(() => {
        this.configuratorDisplayed.set(false);
        this.settingsPopupDisplayed.set(true);
    });
    hideSettingsPopup = action(() => {
        this.settingsPopupDisplayed.set(false);
    });
    getPersistentSettings = action(() => {
        return {
            terminalSettings: this.terminalSettings,
            blinkingTitleEnabled: this.blinkingTitleEnabled.get()
        }
    });
    setSpecialTitle = action((string: string) => {
        this.specialPageTitle.set(`${string}`)
    });
    clearSpecialTitle = action(() => {
        this.specialPageTitle.set(undefined);
    });

    // todo
    applyPersistentSettings = (settings: any) => {
        if (settings.terminalSettings) {
            const {minDisplayLength, minHistoryLength, echoStyle} = settings.terminalSettings;

            if (Number.isInteger(minHistoryLength)) {
                const validMinHistoryLength = Math.max(0, Number(minHistoryLength));
                this.setMinHistoryLength(validMinHistoryLength, false);
            }

            if (Number.isInteger(minDisplayLength)) {
                const validMinDisplayLength = Math.max(50, Number(minDisplayLength));
                this.setMinDisplayLength(validMinDisplayLength, false);
            }

            if (Array.isArray(echoStyle)) {
                const cleanStyle = echoStyle.filter(item => Number.isInteger(item));
                this.setEchoStyle(cleanStyle, false);
            }
        }

        if (typeof settings.blinkingTitleEnabled === 'boolean') {
            this.setBlinkingTitleEnabled(settings.blinkingTitleEnabled);
        }
    };

    savePersistentSettingsToLocalStorage = () => {
        window.localStorage.setItem('settings', JSON.stringify(this.getPersistentSettings()));
        window.localStorage.setItem('settings_version', "1");
    };

    setMinDisplayLength = action((length: number, updateLocalStorage: boolean = true) => {
        this.terminalSettings.minDisplayLength = length;

        if (updateLocalStorage) {
            this.savePersistentSettingsToLocalStorage();
        }
    });
    setMinHistoryLength = action((length: number, updateLocalStorage: boolean = true) => {
        this.terminalSettings.minHistoryLength = length;

        if (updateLocalStorage) {
            this.savePersistentSettingsToLocalStorage();
        }
    });
    setEchoStyle = action((echoStyle: number[], updateLocalStorage: boolean = true) => {
        this.terminalSettings.echoStyle = echoStyle;

        if (updateLocalStorage) {
            this.savePersistentSettingsToLocalStorage();
        }
    });
    setBlinkingTitleEnabled = action((value: boolean, updateLocalStorage: boolean = true) => {
        this.blinkingTitleEnabled.set(value);

        if (updateLocalStorage) {
            this.savePersistentSettingsToLocalStorage();
        }
    });

    loadPersistentSettingsFromLocalStorage = () => {
        const persistentSettingsString = window.localStorage.getItem('settings');
        if (persistentSettingsString) {
            try {
                const persistentSettings = JSON.parse(persistentSettingsString);
                this.applyPersistentSettings(persistentSettings);
            } catch (e) {
                window.localStorage.removeItem('settings');
            }
        }
    };
}

let appState = new AppState();

export default appState;