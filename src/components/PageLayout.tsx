// @flow

import React, {Component, Fragment} from "react";
import styled from "styled-components";
import {observer} from "mobx-react";
import Div100vh from 'react-div-100vh'

import {ErrorMessages, MudTerminal, RightColumn, LeftColumn, HamburgerMenu, HotkeyConfigurator} from "./index";
import {checkBrowser, createMailtoUrl} from "../functions";
import hotkeyManager from "../hotkeyManager";
import appState from "../appState";
import type {AppState} from "../appState";
import {MenuItem} from './Menu';
import SettingsPopup from "./SettingsPopup";
import createBugReportTemplate from "../functions/createBugReportTemplate";

const layoutStyleBreakPoint = "85rem";
const terminalMaxWidth = "51rem";

const VerticalWrapper = styled(Div100vh)`
  display: flex;
  flex-direction: column;
`;

const Layout = styled.div`
  position: relative;
  left: 0;
  top: 0;
  
  width: 100%;
  flex: 1;
  align-items: stretch;
  display: flex;
  justify-content: center;
  
  @media (min-width: ${layoutStyleBreakPoint}) {
      
  }
`;

const LeftColumnWrapper = styled.div`
  flex: 1;
  display: none;
  
  @media (min-width: ${layoutStyleBreakPoint}) {
    display: flex;
  }
`;
const RightColumnWrapper = styled.div`
  flex: 1;
  display: none;
  position: relative;
  
  @media (min-width: ${layoutStyleBreakPoint}) {
    display: flex;
  }
`;
const TerminalWrapper = styled.div`  
  font-size: 2vw;
  margin: 0 auto;
  overflow-y: auto;
  overflow-x: hidden;
  
  flex-grow: 0;
  flex-shrink: 0;
  
  width: 100%;
  max-width: ${terminalMaxWidth};
  
  display: flex;
  padding: 0 1em;
  
  @media (min-width: ${terminalMaxWidth}) {
    padding: 0 0em;
    font-size: 1em;
  }
`;

const HamburgerMenuWrapper = styled.div`
  @media (min-width: ${layoutStyleBreakPoint}) {
    display: none;
  }
`;

type Props = {
    appState: AppState
};
type State = {
    errorMessages: Array<string>,
    supported: boolean,
    showConfigurator: boolean
};

class PageLayout extends Component<Props, State> {
    state: State;
    topItems: Array<MenuItem>;
    bottomItems: Array<MenuItem>;
    externalItems: Array<MenuItem>;
    allMenuItems: Array<MenuItem>;

    constructor(props: Props) {
        super(props);

        this.state = {errorMessages: [], supported: false, showConfigurator: false};

        this.topItems = [
            new MenuItem("Nastavit makra", undefined, this.props.appState.displayHotkeyConfigurator),
            new MenuItem("Další nastavení", undefined, this.props.appState.displaySettingsPopup)
        ];

        this.bottomItems = [];

        const email = process.env.REACT_APP_MAINTAINER_EMAIL;
        if (email) {
            this.bottomItems.push(
                new MenuItem("Navrhnout vylepšení",
                    createMailtoUrl(email, 'Nápad k webovému klientovi')
                )
            );
            this.bottomItems.push(
                new MenuItem("Nahlásit chybu",
                    createMailtoUrl(
                        email,
                        'Chyba ve webovém klientovi',
                        createBugReportTemplate())
                )
            );
        }

        this.externalItems = [
            new MenuItem("Prahy na Wikipedii", "https://cs.wikipedia.org/wiki/Prahy"),
            new MenuItem("Klub Prahy na Okounu", "https://www.okoun.cz/boards/prahy"),
            new MenuItem("Prahy na Wikiverzitě", "https://cs.wikiversity.org/wiki/Prahy")
        ];

        this.allMenuItems = [...this.topItems, ...this.bottomItems, ...this.externalItems];
    }

    componentDidMount(): void {
        this.setState({supported: checkBrowser(this.props.appState.addErrorMessage)});
    }

    render() {
        return (
            <Fragment>
                <VerticalWrapper>
                    <ErrorMessages appState={appState}/>
                    <Layout>
                        <LeftColumnWrapper>
                            <LeftColumn topItems={this.topItems} bottomItems={this.bottomItems} externalItems={this.externalItems}/>
                        </LeftColumnWrapper>
                        <TerminalWrapper>
                            {this.state.supported ?
                                <MudTerminal hotkeyManager={hotkeyManager} appState={appState}/> : null}
                        </TerminalWrapper>
                        <RightColumnWrapper>
                            <RightColumn/>
                        </RightColumnWrapper>
                        <HamburgerMenuWrapper>
                            <HamburgerMenu items={this.allMenuItems}/>
                        </HamburgerMenuWrapper>
                    </Layout>
                </VerticalWrapper>
                {
                    this.props.appState.configuratorDisplayed.get() ?
                        <HotkeyConfigurator hotkeyManager={hotkeyManager} appState={appState}/>
                        : null
                }
                {
                    this.props.appState.settingsPopupDisplayed.get() ?
                        <SettingsPopup hotkeyManager={hotkeyManager} appState={appState}/>
                        : null
                }
            </Fragment>
        );
    }
}

export default observer(PageLayout);