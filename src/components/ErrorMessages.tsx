// @flow

import React from "react";
import styled from "styled-components";
import CloseButton from './CloseButton';
import {AppState} from "../appState";
import {observer} from "mobx-react";

const MessageContainer = styled.div`
  width: 100%;
  flex: 0;
`;
const Message = styled.div`
  background: #ee2e31;
  color: white;
  
  display: flex;
`;

const MessageContent = styled.div`
  padding: 0.5em;

  flex: 1;
`;

type Props = {
    appState: AppState
}

const ErrorMessages = (props: Props) => (
    <MessageContainer>
        {props.appState.errorMessages.map((text, index) => (
            <Message key={index}>
                <MessageContent><strong>Chyba:</strong> {text}</MessageContent>
                <CloseButton onClick={() => props.appState.closeErrorMessage(index.toString())}/>
            </Message>
        ))}
    </MessageContainer>
);

export default observer(ErrorMessages);