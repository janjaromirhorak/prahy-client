import React from "react";
import styled from "styled-components";

import {MdClose} from "react-icons/md";

const StyledButton = styled.button`
  border: none;
  background: none;
  cursor: pointer;
  
  color: white;
    
  padding-top: 0.2em;
  
  opacity: 0.5;
    
  &:hover {
    opacity: 1;
  }
`;

const CloseIcon = styled(MdClose)`
  font-size: 2em;
`;

interface Props {
    onClick?: {(e: MouseEvent): void}
}

const CloseButton = ({onClick}: Props) => (
    // @ts-ignore
    <StyledButton onClick={onClick}><CloseIcon/></StyledButton>
);

export default CloseButton;