// @flow

import {Component} from "react";
import {observer} from "mobx-react";
import type {AppState} from "../appState";

type Props = {
    appState: AppState
};
type State = {
    pageIsVisible: boolean,
    specialTitleIsVisible: boolean,
};

class TitleManager extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {
            pageIsVisible: this.pageIsVisible(),
            specialTitleIsVisible: true
        }
    }

    componentDidMount(): void {
        document.addEventListener("visibilitychange", this.handleVisibilityChange, false);
    }

    componentWillUnmount(): void {
        document.removeEventListener("visibilitychange", this.handleVisibilityChange);
    }

    pageIsVisible() {
        return document.visibilityState === "visible";
    }

    handleVisibilityChange = () => {
        const visible = this.pageIsVisible();
        this.setState({
            pageIsVisible: visible
        });

        this.props.appState.clearSpecialTitle();
    };

    getTitle = () => {
        const defaultTitle = process.env.REACT_APP_DEFAULT_TITLE ?? "";

        if(this.props.appState.blinkingTitleEnabled.get()) {
            if (!this.state.pageIsVisible && this.state.specialTitleIsVisible) {
                const specialTitle = this.props.appState.specialPageTitle.get();
                if (specialTitle) {
                    return `${specialTitle} | ${defaultTitle}`;
                }
            }
        }

        return defaultTitle;
    };

    // set the page title and render nothing
    render() {
        document.title = this.getTitle();
        return null;
    }
}

export default observer(TitleManager);
