// @flow

import styled from "styled-components";

export const MudContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
`;

export const OutputWrapper = styled.div`
    flex: 1;
    position:relative;
`;

export const Output = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    overflow-x: hidden;
    overflow-y: scroll;
    
    scrollbar-color: rgba(255, 255, 255, 0.5) black;
`;

export const InputWrapper = styled.div`
    flex: 0;
`;

export const Input = styled.textarea`
    color: white;
    background: black;
    width: 100%;
    border: solid 2px rgba(255, 255, 255, 0.5);
    height: 2em;
    padding: 0.3em;
    
    font-family: 'Courier Prime', monospace;
    
    resize: none;
    
    &:hover, &:active, &:focus {
      border-color: white;
    }
`;

export const ReconnectButton = styled.button`
    color: white;
    background: black;
    width: 100%;
    border: solid 2px white;
    height: 2em;
    cursor: pointer;
    
    &:hover {
        background: white;
        color: black;
    }
    
    font-family: 'Courier Prime', monospace;
`;

export const Connecting = styled.div`
  height: 2em;
  text-align: center;
  opacity: 0.8;
`;