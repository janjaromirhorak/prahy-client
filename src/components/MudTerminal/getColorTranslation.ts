import colorMap from './colorMap';

const translations = new Map();
translations.set(30, "černá");
translations.set(31, "červená");
translations.set(32, "zelená");
translations.set(33, "žlutá");
translations.set(34, "modrá");
translations.set(35, "purpurová");
translations.set(36, "tyrkysová");
translations.set(37, "bílá");

const colorKeys = Object.keys(colorMap).map(key => Number(key));

const getColorTranslation = (colorCode: string) => {
    const code = Number(colorCode);
    if (colorKeys.includes(code)) {
        return translations.get(code);
    }

    return undefined;
};

export default getColorTranslation;