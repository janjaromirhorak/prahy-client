// @flow

import React, {ChangeEvent, Component, FormEvent} from 'react';
import {observer} from "mobx-react";

// @ts-ignore
import AnsiParser from 'node-ansiparser';
import {InView} from 'react-intersection-observer';

import {Connecting, Input, InputWrapper, MudContainer, Output, OutputWrapper, ReconnectButton} from './styled'
import TelnetParser from "./TelnetParser";
import {defaultSgrCodes, applySgrCodes} from "./sgrStyleUtilities";
import {sgrCodesToCssStyles} from "./StyledText";
import StyledText from "./StyledText";
import {HotkeyManager} from "../../hotkeyManager";

import {hotkeyMapping as defaultHotkeys} from "../../defaultConfig";
import {AppState} from "../../appState";

const T_IAC = 255;
const T_WILL = 251;
const T_WONT = 252;
const T_DO = 253;
const T_DONT = 254;
const T_ECHO = 1;
const T_SUPPRESS_GO_AHEAD = 3;

// 30 seconds before the next keepAlive is sent
const keepAliveInterval = 30 * 1000;

type Props = {
    hotkeyManager: HotkeyManager,
    appState: AppState
};
type State = {
    display: any[], // todo
    bufferedDisplay: any, // todo
    currentSgrCodes: Array<number>,
    bufferedBinary: Uint8Array,
    input: string,
    connected: boolean,
    connecting: boolean,
    history: Array<string>,
    historyPosition: number,
    shouldEchoCommands: boolean,
    shouldScrollToEnd: boolean,
    nextKeepAlive?: number
};

class Terminal extends Component<Props, State> {
    outputRef: any;
    socket?: WebSocket;
    ansiParser: AnsiParser;
    bufferedAnsiParser: AnsiParser;
    telnetParser: TelnetParser;
    textEncoder: TextEncoder;
    textDecoder: TextDecoder;
    textCounter: number;

    constructor(props: Props) {
        super(props);

        // reference to the scrolling div with the output
        // used for autoscroll to the bottom
        this.outputRef = React.createRef();

        // counter used to generate unique indices for each line of text
        this.textCounter = 0;

        this.state = {
            display: [],
            bufferedDisplay: <StyledText key={this.getTextIndex()}/>,
            currentSgrCodes: defaultSgrCodes,
            bufferedBinary: new Uint8Array([]),
            input: "",
            connected: false,
            connecting: true,
            history: [],
            historyPosition: 0,
            shouldEchoCommands: true,
            shouldScrollToEnd: true
        };

        // websocket used for all communication with the backend
        this.socket = undefined;

        // parses the string and calls callbacks defined in the parser
        this.ansiParser = this.getAnsiParser();

        // the same as ansiParser but for the last unfinished line of output
        this.bufferedAnsiParser = this.getAnsiParser(true);

        // parses telnet commands and responds to them
        this.telnetParser = this.getTelnetParser();

        // UTF-8-to-binary and binary-to-UTF-8 conversion
        this.textEncoder = new TextEncoder();
        this.textDecoder = new TextDecoder();

        // (re)connect to the socket on init
        this.reconnect();
    }

    componentDidMount(): void {
        // register a hotkey for the reconnect button
        this.props.hotkeyManager.registerHotkeyAction(defaultHotkeys.reconnect, (event) => {
            if (!this.state.connected && !this.state.connecting) {
                event.preventDefault();
                this.onReconnectButtonClick();
            }
        });

        // register a callback that is used when user presses a registered terminal macro key
        this.props.hotkeyManager.registerTerminalMacroCallback(this.terminalMacroCallback);
    }

    /**
     * Callback for the IntersectionObserver that detects if the terminal is scrolled to the bottom.
     *
     * @param inView - whether the terminal is scrolled to the bottom
     */
    observerCallback = (inView: boolean) => {
        this.setState({shouldScrollToEnd: inView});
    };

    /**
     * Returns a unique index for a line of text
     *
     * @returns {number}
     */
    getTextIndex = () => {
        return this.textCounter++;
    };

    /**
     * Callback for keyboard macros. Prevents the default key action and executes the command.
     *
     * @param event
     * @param command
     */
    terminalMacroCallback = (event: KeyboardEvent, command: string) => {
        if (this.state.connected) {
            event.preventDefault();
            this.runCommand(command);
        }
    };

    componentWillUnmount(): void {
        // unregister the reconnect hotkey
        this.props.hotkeyManager.unregisterHotkey('reconnect');

        window.clearTimeout(this.state.nextKeepAlive);
    }

    /**
     * Returns an initialized ANSI parser. If bufferedParser is true,
     * different callback will be executed for certain actions.
     *
     * @param bufferedParser
     * @returns {AnsiParser}
     */
    getAnsiParser(bufferedParser = false) {
        const onPrint = bufferedParser ? this.onBufferedTermPrint : this.onTermPrint;
        const onExecute = bufferedParser ? () => {
            console.error("Undefined buffered execute!")
        } : this.onTermExecute;

        const onCSI = bufferedParser ? () => {} : this.onTermCSI;

        const terminal = {
            inst_p: onPrint,
            inst_o: function (s: any) {
                console.log('osc', s);
            },
            inst_x: onExecute,
            inst_c: onCSI,
            inst_e: function (collected: any, flag: any) {
                console.log('esc', collected, flag);
            },
            inst_H: function (collected: any, params: any, flag: any) {
                console.log('dcs-Hook', collected, params, flag);
            },
            inst_P: function (dcs: any) {
                console.log('dcs-Put', dcs);
            },
            inst_U: function () {
                console.log('dcs-Unhook');
            }
        };

        return new AnsiParser(terminal);
    }

    /**
     * Returns an initialized TelnetParser with hooks for certain commands.
     *
     * @returns {TelnetParser}
     */
    getTelnetParser() {
        let telnetParser = new TelnetParser();

        telnetParser.addCommand([T_IAC, T_WILL, T_ECHO], this.onIacWillEcho);
        telnetParser.addCommand([T_IAC, T_DO, T_ECHO], () => {
            console.log("IAC DO ECHO")
        });
        telnetParser.addCommand([T_IAC, T_WONT, T_ECHO], this.onIacWontEcho);
        telnetParser.addCommand([T_IAC, T_DONT, T_ECHO], () => {
            console.log("IAC DONT ECHO")
        });
        telnetParser.addCommand([T_IAC, T_WONT, T_SUPPRESS_GO_AHEAD], () => {
            console.log("IAC WONT SUPPRESS GO AHEAD");
        });

        return telnetParser;
    }

    /**
     * Callback for the IAC WILL ECHO telnet command.
     * Disables command echoing and sends an affirmation to the backend.
     */
    onIacWillEcho = () => {
        this.sendBinaryData(new Uint8Array([T_IAC, T_DO, T_ECHO]));
        this.setState({shouldEchoCommands: false});
    };

    /**
     * Callback for the IAC WONT ECHO telnet command
     * Enables command echoing and sends an affirmation to the backend.
     */
    onIacWontEcho = () => {
        this.sendBinaryData(new Uint8Array([T_IAC, T_DONT, T_ECHO]));
        this.setState({shouldEchoCommands: true});
    };

    /**
     * Callback for the print action of the buffered ANSI terminal.
     * Adds the provided string to a temporary line on the end of the output.
     *
     * @param string
     */
    onBufferedTermPrint = (string: string) => {
        const {children, ...rest} = this.state.bufferedDisplay.props;
        this.setState({
            bufferedDisplay: <StyledText {...rest} key={this.getTextIndex()}>{children}{string}</StyledText>
        })
    };

    /**
     * Callback for the print action of the standard ANSI terminal.
     * Adds the provided string to the output.
     *
     * @param string
     */
    onTermPrint = (string: string) => {
        let {display} = this.state;

        const style = sgrCodesToCssStyles(this.state.currentSgrCodes);

        display.push(<StyledText
            key={this.getTextIndex()}
            className={style.join(" ")}>
            {string}
        </StyledText>);

        const {minDisplayLength} = this.props.appState.terminalSettings;

        if (display.length > minDisplayLength * 1.5) {
            display = display.slice(display.length - minDisplayLength);
        }

        this.setState({display});

        this.props.appState.setSpecialTitle(string);
    };

    /**
     * Callback for the execute action of the ANSI terminal
     * @param flag
     */
    onTermExecute = (flag: string) => {
        if (flag === "\n") {
            let {display} = this.state;

            if (display.length > 0) {
                let {children, ...rest} = display[display.length - 1].props;
                display[display.length - 1] =
                    <StyledText {...rest} key={this.getTextIndex()}>{children}<br/></StyledText>;

            } else {
                display = [<StyledText key={this.getTextIndex()}
                                       className={sgrCodesToCssStyles(this.state.currentSgrCodes).join(" ")}><br/></StyledText>];
            }

            this.setState({display});
        } else if (flag === "\r") {
            // CR ignorujeme
        } else {
            console.log('execute', flag.charCodeAt(0));
        }
    };

    /**
     * Callback for the CSI action of the ANSI terminal
     *
     * @param collected
     * @param params
     * @param flag
     */
    onTermCSI = (collected: any, params: number[], flag: string) => {
        // http://www.primidi.com/ansi_escape_code/csi_codes
        if (flag === "J") {
            // clear screen
            this.setState({display: []});
        } else if (flag === "H") {
            // move cursor - this does not apply for this implementation
        } else if (flag === "m") {
            // Sets SGR parameters, including text color. After CSI can be zero or more parameters separated with ;.
            // With no parameters, CSI m is treated as CSI 0 m (reset / normal), which is typical of most of the ANSI escape sequences.
            // https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_parameters
            if (params.length === 0) {
                params = [0];
            }

            this.setState({
                currentSgrCodes: applySgrCodes(params, this.state.currentSgrCodes)
            })
        } else {
            console.warn('unknown CSI code:', collected, params, flag)
        }
    };

    componentDidUpdate() {
        if (this.state.shouldScrollToEnd) {
            this.scrollToEnd();
        }
    }

    /**
     * Scrolls the terminal output to the bottom
     */
    scrollToEnd = () => {
        const out = this.outputRef.current;
        out.scrollTop = out.scrollHeight;
    };

    /**
     * Reconnect to the backend websocket and assign open, close, error and message callbacks.
     */
    reconnect = () => {
        const address = process.env.REACT_APP_WEBSOCKET_ADDRESS;
        if (address) {
            this.socket = new WebSocket(address);
            this.socket.onopen = this.onSocketOpen;
            this.socket.onclose = this.onSocketClose;
            this.socket.onerror = this.onSocketError;
            this.socket.onmessage = this.onSocketMessage;
        } else {
            this.props.appState.addErrorMessage("Chyba při načítání websocketové adresy Prahů.");
        }
    };

    /**
     * Callback for the opening of the websocket
     */
    onSocketOpen = () => {
        this.setState({connecting: false, connected: true});

        // automatically choose utf-8
        this.sendSocketMessage("u");

        this.scheduleKeepAlive();
    };

    /**
     * Callback for the closing of the websocket
     */
    onSocketClose = () => {
        this.setState({connecting: false, connected: false});
    };

    /**
     * Callback for an error in the websocket connection
     */
    onSocketError = () => {
        this.setState({connecting: false});

        this.props.appState.addErrorMessage("Připojení k Prahům selhalo.");
    };

    /**
     * Callback for a received message from the websocket
     * @param event
     */
    onSocketMessage = (event: MessageEvent) => {
        const reader = new FileReader();

        // $FlowFixMe
        reader.readAsArrayBuffer(event.data);
        reader.onloadend = () => {
            // @ts-ignore
            this.processResponseBuffer(reader.result);
        };
    };

    /**
     * Iterates over array buffer and parses all IAC WILL X / IAC DO X commands using a telnet parser.
     * The rest is then processed with ANSI parsers. The last unfinished line is kept as a buffer
     * and processed differently in case a UTF-8 multi-byte character gets divided between two separate messages.
     *
     * @param arrayBuffer
     */
    processResponseBuffer(arrayBuffer: ArrayBuffer, forceFlushBuffered = false) {
        let uintView = new Uint8Array(arrayBuffer);

        if (this.state.bufferedBinary.length > 0) {
            let mergedArray = new Uint8Array(this.state.bufferedBinary.length + uintView.length);
            mergedArray.set(this.state.bufferedBinary, 0);
            mergedArray.set(uintView, this.state.bufferedBinary.length);
            arrayBuffer = mergedArray.buffer;
            uintView = new Uint8Array(arrayBuffer);

            this.setState({
                bufferedDisplay: <StyledText key={this.getTextIndex()}/>,
                bufferedBinary: new Uint8Array(0)
            });
        }

        let lastBreak = 0;
        for (let i = 0; i < uintView.length; i++) {
            this.telnetParser.resetCurrentNode();

            let offset = 0;
            while (true) {
                if (this.telnetParser.traverseToChild(uintView[i + offset])) {
                    if (this.telnetParser.currentNodeIsLeaf()) {
                        const action = this.telnetParser.getCurrentNodeAction();
                        if (action !== undefined) {
                            action();
                        }

                        // shift the data to remove this telnet command from the arrayBuffer
                        for (let j = i; j < uintView.length - offset - 1; j++) {
                            uintView[j] = uintView[j + 3];
                        }

                        // strip last "offset" bytes from the buffer
                        uintView = uintView.slice(0, uintView.length - offset - 1);
                        arrayBuffer = arrayBuffer.slice(0, arrayBuffer.byteLength - offset - 1);

                        i--;

                        break;
                    }

                    offset++;
                } else {
                    break;
                }
            }

            if (uintView[i] === 10) {
                const string = this.textDecoder.decode(uintView.slice(lastBreak, i + 1));
                this.ansiParser.parse(string);
                lastBreak = i + 1;
            }
        }

        if (lastBreak !== uintView.length) {
            if(forceFlushBuffered) {
                const string = this.textDecoder.decode(uintView.slice(lastBreak, uintView.length));
                this.setState({bufferedBinary: new Uint8Array(0)});
                this.ansiParser.parse(string);
            } else {// this is the last bit that may end with an uncomplete UTF-8 character
                this.setState({bufferedBinary: uintView.slice(lastBreak, uintView.length)});
                const string = this.textDecoder.decode(uintView.slice(lastBreak, uintView.length));
                this.bufferedAnsiParser.parse(string);
            }
        }
    }

    /**
     * Sends a string over the websocket
     *
     * @param text
     */
    sendSocketMessage = (text: string) => {
        if (this.socket) {
            const blob = new Blob([`${text}\n`], {type: "text/plain"});
            this.socket.send(blob);
        }
    };

    /**
     * Sends a binary message over the websocket
     *
     * @param uints
     */
    sendBinaryData = (uints: Uint8Array) => {
        if (this.socket) {
            const blob = new Blob([uints]);
            this.socket.send(blob);
        }
    };

    /**
     * Sends a keepalive message to maintain connection with the bridge
     */
    sendKeepAlive = () => {
        this.sendBinaryData(new Uint8Array([]))
    };

    scheduleKeepAlive = () => {
        window.clearTimeout(this.state.nextKeepAlive);
        this.setState({
            nextKeepAlive: window.setTimeout(() => {
                this.sendKeepAlive();
                this.scheduleKeepAlive();
            }, keepAliveInterval)
        });
    };

    /**
     * Callback to handle when user changes the terminal input element contents
     *
     * @param target
     */
    onInputChange = ({target}: ChangeEvent<HTMLInputElement>) => {
        this.setState({input: target.value});
    };

    /**
     * Adds a command to the command history
     *
     * @param command
     */
    addCommandToHistory = (command: string) => {
        let {history} = this.state;
        history = history.filter(cmd => cmd !== command);
        history.push(command);

        const {minHistoryLength} = this.props.appState.terminalSettings;
        if (history.length > minHistoryLength) {
            history = history.slice(history.length - minHistoryLength);
        }

        this.setState({history});
    };

    /**
     * Echoes a command to the output
     *
     * @param command
     */
    echoCommand = (command: string) => {
        this.processResponseBuffer(new ArrayBuffer(0), true);

        let {display} = this.state;

        display.push(<StyledText
            key={this.getTextIndex()}
            className={sgrCodesToCssStyles(this.props.appState.terminalSettings.echoStyle).join(" ")}>
            {command + "\n"}
        </StyledText>);

        this.setState({display});
    };

    /**
     * Callback for when the user submits a command
     *
     * @param e
     */
    onInputSubmit = (e: FormEvent<HTMLFormElement> | KeyboardEvent) => {
        e.preventDefault();

        const commands = this.state.input.replace("\r", "").split("\n");

        for(const command of commands) {
            this.runCommand(command);
        }

        this.setState({
            input: "",
            historyPosition: 0
        });

        this.scrollToEnd();
    };

    /**
     * Execute a command by sending it to the backend
     *
     * @param command
     */
    runCommand = (command: string) => {
        if (this.state.shouldEchoCommands) {
            this.echoCommand(command);
            if (command.length > 0) {
                this.addCommandToHistory(command);
            }
        }

        this.sendSocketMessage(command);
    };

    /**
     * Handler for keypresses in the input field. Used to browse the command history.
     *
     * @param e
     */
    onInputKeyPress = (e: KeyboardEvent) => {
        if (e.key === "ArrowUp" || e.key === "ArrowDown") {
            const {historyPosition, history} = this.state;
            const direction = e.key === "ArrowUp" ? 1 : -1;
            const newPosition = Math.min(Math.max(0, historyPosition + direction), history.length);
            this.setState({
                historyPosition: newPosition,
                input: newPosition === 0 ? "" : history[history.length - newPosition]
            });
        } else if (e.key === "Enter") {
            e.preventDefault();
            this.onInputSubmit(e);
        }
    };

    onReconnectButtonClick = () => {
        if (!this.state.connected && !this.state.connecting) {
            this.setState({connecting: true});
            this.reconnect();
        }
    };

    render() {
        const displayContent = [...this.state.display, this.state.bufferedDisplay];

        const reconnectKey = this.props.hotkeyManager.getHotkeyDescription('reconnect');
        return (
            <MudContainer>
                <OutputWrapper>
                    <Output ref={this.outputRef}>
                        {displayContent}
                        <InView as="div" onChange={this.observerCallback}>
                            <div/>
                        </InView>
                    </Output>
                </OutputWrapper>
                <InputWrapper>
                    {this.state.connected ? (
                        <form onSubmit={this.onInputSubmit}>
                            {/* @ts-ignore */}
                            <Input
                                // @ts-ignore
                                onChange={this.onInputChange}
                                // @ts-ignore
                                onKeyDown={this.onInputKeyPress}
                                // @ts-ignore
                                autoFocus
                                // @ts-ignore
                                autoCapitalize="none"
                                // @ts-ignore
                                autoCorrect="off"
                                // @ts-ignore
                                autoComplete="off"
                                // @ts-ignore
                                spellCheck="false"
                                // @ts-ignore
                                value={this.state.input}
                            />
                        </form>
                    ) : (
                        this.state.connecting ? (
                            <Connecting>připojuji k Prahům...</Connecting>
                        ) : (
                            <ReconnectButton onClick={this.onReconnectButtonClick}>
                                připojit k Prahům
                                {reconnectKey ? ` <${reconnectKey}>` : null}
                            </ReconnectButton>
                        )
                    )}
                </InputWrapper>
            </MudContainer>
        )
    }
}

export default observer(Terminal);