// @flow

export const defaultSgrCodes = [0];

/**
 * Returns true if the SGR code matches a foreground color instruction
 */
export const isForegroundColor = (code: number): boolean => {
    return (code >= 30 && code <= 37) || (code >= 90 && code <= 97);
};

/**
 * Returns true if the SGR code matches a background color instruction
 */
export const isBackgroundColor = (code: number): boolean => {
    return (code >= 40 && code <= 47) || (code >= 100 && code <= 107);
};

/**
 * Applies the specified SGR code to an array of SGR codes. Returns an array of SGR codes.
 */
export const applySgrCode = (code: number, style: number[]): number[] => {
    if (code === 0) {
        return [0];
    } else {
        let newStyle = style;

        if (isForegroundColor(code)) {
            newStyle = newStyle.filter(code => !isForegroundColor(code));
        } else if (isBackgroundColor(code)) {
            newStyle = newStyle.filter(code => !isBackgroundColor(code));
        }

        return [...new Set<number>([...newStyle, code])];
    }
};

/**
 * Applies all the specified SGR code to an array of SGR codes. Returns an array of SGR codes.
 */
export const applySgrCodes = (codes: Iterable<number>, style: number[]): number[] => {
    for (const code of codes) {
        style = applySgrCode(code, style);
    }
    return style;
};

/**
 * Applies a SGR code to an array of codes, only if the specified code is a foreground code.
 * Passing 0 as code causes a foreground color reset.
 */
export const applyForegroundColor = (code: number, style: number[]): number[] => {
    if (code === 0) {
        return style.filter(code => !isForegroundColor(code))
    }

    if (isForegroundColor(code)) {
        return applySgrCode(code, style);
    }

    return style;
};

/**
 * Applies a SGR code to an array of codes, only if the specified code is a background code.
 * Passing 0 as code causes a background color reset.
 */
export const applyBackgroundColor = (code: number, style: number[]): number[] => {
    if (code === 0) {
        return style.filter(code => !isBackgroundColor(code))
    }

    if (isBackgroundColor(code)) {
        return applySgrCode(code, style);
    }

    return style;
};

/**
 * Returns a SGR code that defines the foreground color or undefined if there is no such code.
 */
export const getActiveForegroundColorCode = (codes: number[]): number | undefined => {
    return codes.reduce((accumulator: number | undefined, current: number) => {
        if (isForegroundColor(current)) {
            return current;
        }

        if (current === 0) {
            return undefined;
        }

        return accumulator;
    }, undefined);
};

/**
 * Returns a SGR code that defines the background color or undefined if there is no such code.
 */
export const getActiveBackgroundColorCode = (codes: number[]): number | undefined => {
    return codes.reduce((accumulator: number | undefined, current: number) => {
        if (isBackgroundColor(current)) {
            return current;
        }

        if (current === 0) {
            return undefined;
        }

        return accumulator;
    }, undefined);
};

/**
 * Removes a specified SGR code from the array of codes
 */
export const removeSgrCode = (code: number, style: number[]):number[] => {
    return style.filter(testedCode => testedCode !== code);
};

/**
 * Returns true only if the array of codes contains the specified SGR code
 */
export const hasSgrCode = (code: number, style: number[]): boolean => {
    return style.reduce((accumulator: boolean, current: number) => (
        (accumulator && (current !== 0)) || current === code
    ), false);
};

/**
 * If the array of codes contains a code, removes this code from the array.
 * Otherwise adds the code to the array.
 */
export const toggleSgrCode = (code: number, style: number[]): number[] => {
    if (hasSgrCode(code, style)) {
        return removeSgrCode(code, style);
    } else {
        return applySgrCode(code, style);
    }
};