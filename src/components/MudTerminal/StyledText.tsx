// @flow

import styled from "styled-components";
import {sgrStyle as defaultStyle} from '../../defaultConfig';
import colorMap from "./colorMap"

const basicKeys = Object.keys(colorMap).map(key => Number(key));

interface Props {
    style?: typeof defaultStyle
}

const colorDefinitions = (props: Props) => basicKeys.map((key) => {
    const colorName: string | undefined = colorMap[key.toString() as keyof typeof colorMap];

    if (colorName) {
        const brightColorName = "bright" + colorName.charAt(0).toUpperCase() + colorName.slice(1);
        const colorCode = props.style ? props.style[colorName as keyof typeof defaultStyle] : defaultStyle[colorName as keyof typeof defaultStyle];
        const brightColorCode = props.style ? props.style[brightColorName as keyof typeof defaultStyle] : defaultStyle[brightColorName as keyof typeof defaultStyle];

        return `
                // foreground color
                &.sgr-${key}:not(.sgr-7) {
                  color: ${colorCode}
                }

                // background color
                &.sgr-${key + 10}:not(.sgr-7) {
                  background-color: ${colorCode}
                }

                // bright foreground color
                &.sgr-${key + 60}:not(.sgr-7),
                &.sgr-1.sgr-${key}:not(.sgr-7) {
                  color: ${brightColorCode}
                }

                // bright background color
                &.sgr-${key + 70}:not(.sgr-7),
                &.sgr-1.sgr-${key + 10}:not(.sgr-7) {
                  background-color: ${brightColorCode}
                }

                // inverted foreground color
                &.sgr-7.sgr-${key},
                &.sgr-${key}::selection {
                  background-color: ${colorCode}
                }

                // inverted background color
                &.sgr-7.sgr-${key + 10},
                &.sgr-${key + 10}::selection {
                  color: ${colorCode}
                }

                // inverted bright foreground color
                &.sgr-7.sgr-${key + 60},
                &.sgr-7.sgr-1.sgr-${key},
                &.sgr-${key + 60}::selection,
                &.sgr-1.sgr-${key}::selection {
                  background-color: ${brightColorCode}
                }

                // inverted bright background color
                &.sgr-7.sgr-${key + 70},
                &.sgr-7.sgr-1.sgr-${key + 10},
                &.sgr-${key + 70}::selection,
                &.sgr-1.sgr-${key + 10}::selection {
                  color: ${brightColorCode}
                }
            `;
    }
    return "";
}).join("\n");

export const sgrCodesToCssStyles = (codes: Array<number>): Array<string> => {
    return codes.map(code => `sgr-${code}`);
};

const getColor = (key: keyof typeof defaultStyle, obj: typeof defaultStyle | undefined): string => {
    if (obj) {
        return obj[key];
    }
    return defaultStyle[key];
};

const brightWhite = (props: Props) => getColor("brightWhite", props.style);
const black = (props: Props) => getColor("black", props.style);

export const ColoredText = styled.span<Props>`
    @keyframes blinker {
        0% {
            opacity: 1
        }
        1% {
            opacity: 0
        }
        50% {
            opacity: 0
        }
        51% {
            opacity: 1
        }
    }
    
    &.sgr-0 {
        color: ${brightWhite};
        background: ${black};
    }
    
    &.sgr-7.sgr-0 {
        background: ${brightWhite};
        color: ${black};
    }
    
    &.sgr-1 {
        font-weight: bold;
    }
    
    &.sgr-2 {
        opacity: 0.8;
    }
    
    &.sgr-3 {
        font-style: italic;
    }
    
    &.sgr-4 {
        text-decoration: underline;
    }
    
    &.sgr-5 {
        animation: blinker 1s linear infinite;
    }
    
    &.sgr-9 {
        text-decoration: line-through;
    }
    
    &.sgr-4.sgr-9 {
        text-decoration: line-through underline;
    }
    
    &.sgr-22 {
        opacity: 1;
        font-weight: normal;
    }
    
    &.sgr-23 {
        font-style: normal;
    }
    
    &.sgr-24 {
        text-decoration: none;
    }
    
    &.sgr-24.sgr-9 {
        text-decoration: line-through;
    }
    
    &.sgr-25 {
        animation: none;
    }
    
    &.sgr-29 {
        font-style: normal;
    }
    
    &.sgr-29.sgr-4 {
        text-decoration: underline;
    }
    
    ${props => colorDefinitions(props)}
`;

const StyledText = styled(ColoredText)`
  white-space: pre-wrap;
`;

export default StyledText;