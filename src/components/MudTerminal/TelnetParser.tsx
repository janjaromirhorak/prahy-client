// @flow

class ParserNode {
    key: void | number;
    action: void | {(): void};
    children: Map<number, ParserNode>;

    constructor(key?: number, action?: () => void) {
        this.key = key;
        this.action = action;

        this.children = new Map();
    }

    isLeaf() {
        return this.children.size === 0;
    }

    setAction(action?: () => void) {
        this.action = action;
    }

    getAction() {
        return this.action;
    }

    hasAction() {
        return this.action !== undefined;
    }

    addChild(key: number, action?: () => void): ParserNode {
        let child = this.children.get(key);
        if (child) {
            child.addChild(key, action);
        } else {
            child = new ParserNode(key, action);
            this.children.set(key, child);
        }
        return child;
    }

    hasChild(key: number) {
        return this.children.has(key);
    }

    getChild(key: number): ParserNode | undefined {
        if (this.hasChild(key)) {
            return this.children.get(key);
        }
    }

    getChildCreateIfNotExists(key: number, action?: () => void): ParserNode {
        let child = this.getChild(key);

        if (!child) {
            child = this.addChild(key, action);
        }

        return child;
    }
}

export default class TelnetParser {
    state: Array<number>;
    root: ParserNode;
    currentNode: ParserNode;

    constructor() {
        this.state = [];
        this.root = new ParserNode();
        this.currentNode = this.root;
    }

    addCommand(sequence: Array<number>, action: () => void) {
        let currentNode = this.root;
        for (const key of sequence) {
            currentNode = currentNode.getChildCreateIfNotExists(key);
        }
        currentNode.setAction(action);
    }

    traverseToChild(key: number): boolean {
        const child = this.currentNode.getChild(key);
        if (child) {
            this.currentNode = child;
            return true;
        }

        return false;
    }

    currentNodeIsLeaf(): boolean {
        return this.currentNode.isLeaf();
    }

    getCurrentNodeAction(): void | {(): void} {
        return this.currentNode.getAction();
    }

    resetCurrentNode(): void {
        this.currentNode = this.root;
    }
}