import React from "react";
import styled from "styled-components";
import Tooltip, {TooltipProps} from 'react-tooltip-lite';
import {MdInfo} from "react-icons/md";

const StyledTooltip = styled(Tooltip)`
  display: inline-block;
`;

const InfoIcon = styled(MdInfo)`
  font-size: 1.2em;
  vertical-align: middle;
`;

const CustomTooltip = (props: TooltipProps) => (
    <StyledTooltip {...props}><InfoIcon/></StyledTooltip>
);

export default CustomTooltip;