// @flow

import React, {ChangeEvent, Component} from "react";
import {observer} from "mobx-react";
import styled from "styled-components";

import type {HotkeyManager} from "../hotkeyManager";
import type {AppState} from "../appState";
import Dialog, {DialogHeader} from "./Dialog";

import Tooltip from "./Tooltip";

import {
    applyBackgroundColor,
    applyForegroundColor,
    getActiveBackgroundColorCode, getActiveForegroundColorCode,
    hasSgrCode,
    toggleSgrCode
} from "./MudTerminal/sgrStyleUtilities";

import {sgrCodesToCssStyles} from "./MudTerminal/StyledText";
import {ColoredText} from "./MudTerminal/StyledText";
import colorMap from './MudTerminal/colorMap';

import {StyledInput, StyledSelect, OnOffSwitch} from './UI';
import getColorTranslation from "./MudTerminal/getColorTranslation";

const SettingsTable = styled.table`
  width: 100%;
`;

const SettingsRow = styled.tr``;

const Cell = styled.td`
  padding: 0.2em;
`;

const NameCell = styled(Cell)`
  padding-left: 0;
`;
const ValueCell = styled(Cell)`
  padding-right: 0;
  text-align: center;
  width: 10em;
`;

const PaddedDialogHeader = styled(DialogHeader)`
  padding-top: 2em;
  padding-bottom: 1.5em;
`;

const TextExample = styled.div`
  padding-bottom: 1.5em;
  text-align: center;
`;

type ConfiguratorProps = {
    appState: AppState,
    hotkeyManager: HotkeyManager
}

type State = {
    minHistoryLength: number,
    minDisplayLength: number,
    echoStyle: Array<number>,
    blinkingTitleEnabled: boolean,
}

class Configurator extends Component<ConfiguratorProps, State> {
    constructor(props: ConfiguratorProps) {
        super(props);

        this.state = {
            minHistoryLength: this.props.appState.terminalSettings.minHistoryLength,
            minDisplayLength: this.props.appState.terminalSettings.minDisplayLength,
            echoStyle: this.props.appState.terminalSettings.echoStyle,
            blinkingTitleEnabled: this.props.appState.blinkingTitleEnabled.get()
        };
    }

    onMinHistoryLengthChange = ({target}: ChangeEvent<HTMLInputElement>) => {
        const {value} = target;
        const validValue = Math.max(0, Number(value));
        this.setState({
            minHistoryLength: validValue
        });
        this.props.appState.setMinHistoryLength(validValue);
    };

    onMinDisplayLengthChange = ({target}: ChangeEvent<HTMLInputElement>) => {
        const {value} = target;
        const validValue = Math.max(50, Number(value));
        this.setState({
            minDisplayLength: validValue
        });
        this.props.appState.setMinDisplayLength(validValue);
    };

    onBlinkingTitleEnabledClick = () => {
        const newValue = !this.state.blinkingTitleEnabled;
        this.setState({
            blinkingTitleEnabled: newValue
        });
        this.props.appState.setBlinkingTitleEnabled(newValue);
    };

    setEchoStyle = (style: number[]) => {
        this.props.appState.setEchoStyle(style);
        this.setState({
            echoStyle: style
        });
    };

    onForegroundColorChange = ({target}: ChangeEvent<HTMLSelectElement>) => {
        this.setEchoStyle(applyForegroundColor(Number(target.value), this.state.echoStyle));
    };

    onBackgroundColorChange = ({target}: ChangeEvent<HTMLSelectElement>) => {
        this.setEchoStyle(applyBackgroundColor(Number(target.value), this.state.echoStyle));
    };

    onStyleToggleClick = (toggleCode: number) => {
        this.setEchoStyle(toggleSgrCode(toggleCode, this.state.echoStyle));
    };

    render() {
        const currentForegroundColorCode = getActiveForegroundColorCode(this.state.echoStyle);
        const currentBackgroundColorCode = getActiveBackgroundColorCode(this.state.echoStyle);

        return (
            <Dialog closeCallback={this.props.appState.hideSettingsPopup}
                    header={"Další nastavení"}>
                <SettingsTable>
                    <tbody>
                    <SettingsRow>
                        <NameCell>
                            Aktuality v{"\u00a0"}titulku <Tooltip
                            content={"zda se mají při neaktivitě (třeba při přepnutí na jiný panel prohlížeče) zobrazovat příchozí hlášky v\u00a0titulku stránky"}
                        />
                        </NameCell>
                        <ValueCell>
                            <OnOffSwitch isOn={this.state.blinkingTitleEnabled}
                                         onClick={this.onBlinkingTitleEnabledClick}/>
                        </ValueCell>
                    </SettingsRow>
                    <SettingsRow>
                        <NameCell>
                            Počet příkazů v{"\u00a0"}historii <Tooltip
                            content={"maximální délka historie příkazů"}
                        />
                        </NameCell>
                        <ValueCell>
                            <StyledInput type="number" min={0} step={1} value={this.state.minHistoryLength}
                                        onChange={this.onMinHistoryLengthChange}/>
                        </ValueCell>
                    </SettingsRow>
                    <SettingsRow>
                        <NameCell>
                            Počet řádků s{"\u00a0"}výpisem <Tooltip
                            content={"kolik řádků textu má být udržováno v terminálu -\
                                        hodně dlouhé výpisy mohou zhoršit odezvu klienta"}
                        />
                        </NameCell>
                        <ValueCell>
                            <StyledInput type="number" min={50} step={10} value={this.state.minDisplayLength}
                                        onChange={this.onMinDisplayLengthChange}/>
                        </ValueCell>
                    </SettingsRow>
                    </tbody>
                </SettingsTable>
                <PaddedDialogHeader>
                    Zobrazení příkazů <Tooltip
                    content={"jak se mají zobrazovat příkazy zadané uživatelem"}
                />
                </PaddedDialogHeader>
                <TextExample>
                    <ColoredText className={sgrCodesToCssStyles(this.state.echoStyle).join(" ")}>příklad textu</ColoredText>
                </TextExample>
                <SettingsTable>
                    <tbody>
                    <SettingsRow>
                        <NameCell>
                            barva textu
                        </NameCell>
                        <ValueCell>
                            <StyledSelect onChange={this.onForegroundColorChange} value={currentForegroundColorCode}>
                                <option key={0} value={0}>výchozí</option>
                                {Object.keys(colorMap).map(Number).map(key => (
                                    <option
                                        value={key}
                                        key={key}
                                    >
                                        {getColorTranslation(key.toString())}
                                    </option>)
                                )}
                            </StyledSelect>
                        </ValueCell>
                    </SettingsRow>
                    <SettingsRow>
                        <NameCell>
                            barva pozadí
                        </NameCell>
                        <ValueCell>
                            <StyledSelect onChange={this.onBackgroundColorChange} value={currentBackgroundColorCode}>
                                <option key={0} value={0}>výchozí</option>
                                {Object.keys(colorMap).map(Number).map(key => (
                                    <option
                                        value={key + 10}
                                        key={key}
                                    >
                                        {getColorTranslation(key.toString())}
                                    </option>)
                                )}
                            </StyledSelect>
                        </ValueCell>
                    </SettingsRow>
                    <SettingsRow>
                        <NameCell>
                            výrazně
                        </NameCell>
                        <ValueCell>
                            <OnOffSwitch isOn={hasSgrCode(1, this.state.echoStyle)}
                                         onClick={() => this.onStyleToggleClick(1)}/>
                        </ValueCell>
                    </SettingsRow>
                    <SettingsRow>
                        <NameCell>
                            tmavě
                        </NameCell>
                        <ValueCell>
                            <OnOffSwitch isOn={hasSgrCode(2, this.state.echoStyle)}
                                         onClick={() => this.onStyleToggleClick(2)}/>
                        </ValueCell>
                    </SettingsRow>
                    <SettingsRow>
                        <NameCell>
                            inverzně
                        </NameCell>
                        <ValueCell>
                            <OnOffSwitch isOn={hasSgrCode(7, this.state.echoStyle)}
                                         onClick={() => this.onStyleToggleClick(7)}/>
                        </ValueCell>
                    </SettingsRow>
                    <SettingsRow>
                        <NameCell>
                            blikání
                        </NameCell>
                        <ValueCell>
                            <OnOffSwitch isOn={hasSgrCode(5, this.state.echoStyle)}
                                         onClick={() => this.onStyleToggleClick(5)}/>
                        </ValueCell>
                    </SettingsRow>
                    <SettingsRow>
                        <NameCell>
                            podtrženě
                        </NameCell>
                        <ValueCell>
                            <OnOffSwitch isOn={hasSgrCode(4, this.state.echoStyle)}
                                         onClick={() => this.onStyleToggleClick(4)}/>
                        </ValueCell>
                    </SettingsRow>
                    </tbody>
                </SettingsTable>
            </Dialog>
        );
    }
}

export default observer(Configurator);