// @flow

import React, {ChangeEvent, Component} from "react";
import {observer} from "mobx-react";
import styled from "styled-components";
import {HotkeyManager} from "../hotkeyManager";
import {AppState} from "../appState";
import {keyMacros as defaultMacros, sgrStyle} from "../defaultConfig";
import Dialog from "./Dialog";
import {GeneralButton, NoBorderButton, generalInputStyle} from "./formElements";

const Paragraph = styled.p`
  max-width: 30em;
  margin-bottom: 1em;
`;

const CommandTable = styled.table`
  border-collapse: collapse;
  margin-bottom: 1em;
  width: 100%;

  & th, & td {
    padding: 0.5em;
    border-width: thin;
    border-color: rgba(255, 255, 255, 0.5);
  }
  
  & th {
    border-style: solid;
  }
  
  & td {
    border-style: solid;
  }
  
  & th.no-border, & td.no-border {
    border: none;
  }
  
  & th.no-padding, & td.no-padding {
    padding: 0;
  }  
`;

const DeleteMacro = styled(NoBorderButton)` 
  &:hover {
    color: ${sgrStyle.brightRed}
  }
`;

const SelectKeyButton = styled(GeneralButton)`
  min-width: 11em;
  margin: 0.5em 0.25em 0 0;
`;

const DeleteEverythingButton = styled(GeneralButton)`
  &:hover {
    background: ${sgrStyle.brightRed};
    border-color: ${sgrStyle.brightRed};
  }
`;

const MacroCommandInput = styled.input`
  ${generalInputStyle};
  margin: 0.5em 0.25em 0 0.25em;
`;

const AddMacroButton = styled(GeneralButton)`
  margin: 0.5em 0 0 0.25em;
`;

const BottomButtons = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const ButtonSpacer = styled.div`
  flex: 1;
`;

type CommandRecordProps = {
    command: {
        key: string,
        command: string
    },
    deleteMacro: {(macro: string): void}
}

class CommandRecord extends Component<CommandRecordProps> {
    deleteMacro = () => {
        this.props.deleteMacro(this.props.command.key);
    };

    render() {
        return (
            <tr>
                <td>
                    {this.props.command.key}
                </td>
                <td>
                    {this.props.command.command}
                </td>
                <td className="no-padding">
                    <DeleteMacro onClick={this.deleteMacro}>smazat</DeleteMacro>
                </td>
            </tr>
        );
    }
}

type ConfiguratorProps = {
    appState: AppState,
    hotkeyManager: HotkeyManager
}

type State = {
    newCommand: string,
    detectingHotkey: boolean,
    detectedHotkey?: string
}

class Configurator extends Component<ConfiguratorProps, State> {
    constructor(props: ConfiguratorProps) {
        super(props);

        this.state = {newCommand: "", detectingHotkey: false, detectedHotkey: undefined};
    }

    componentWillUnmount() {
        window.removeEventListener('keydown', this.detectHotkey);
    }

    onNewCommandInputChange = ({target}: ChangeEvent<HTMLInputElement>) => {
        this.setState({newCommand: target.value});
    };

    deleteMacro = (key: string) => {
        this.props.hotkeyManager.unregisterHotkey(key);
    };

    deleteAllMacros = () => {
        const commands = this.getRegisteredCommands();
        for (const command of commands) {
            this.props.hotkeyManager.unregisterHotkey(command.key);
        }
    };

    loadDefaultMacros = () => {
        const keys = Object.keys(defaultMacros);
        for (const key of keys) {
            this.props.hotkeyManager.registerTerminalHotkey(key, defaultMacros[key as keyof typeof defaultMacros]);
        }
    };

    getRegisteredCommands = () => {
        const hotkeys = this.props.hotkeyManager.hotkeys;
        const keys = Object.keys(hotkeys);

        let commands = [];
        for (const key of keys) {
            if (hotkeys[key]) {
                if (hotkeys[key].terminalHotkeyCommand) {
                    commands.push({
                        key,
                        command: hotkeys[key].terminalHotkeyCommand
                    })
                }
            }
        }

        return commands;
    };

    startDetectingHotkey = () => {
        window.addEventListener('keydown', this.detectHotkey);
        this.setState({detectedHotkey: undefined, detectingHotkey: true});
    };

    stopDetectingHotkey = () => {
        window.removeEventListener('keydown', this.detectHotkey);
        this.setState({detectingHotkey: false});
    };

    detectHotkey = (event: KeyboardEvent) => {
        this.stopDetectingHotkey();
        // todo check if not blacklisted
        this.setState({detectedHotkey: event.code});
    };

    addNewMacro = () => {
        const {detectedHotkey, newCommand} = this.state;
        if (detectedHotkey && newCommand) {
            this.props.hotkeyManager.registerTerminalHotkey(detectedHotkey, newCommand);
            this.setState({detectedHotkey: undefined, newCommand: ""})
        }
    };

    render() {
        const commands = this.getRegisteredCommands();

        return (
            <Dialog closeCallback={this.props.appState.hideHotkeyConfigurator} header={"Nastavení maker"}>
                <Paragraph>
                    Zde si můžeš nastavit klávesová makra. Systém zatím podporuje pouze jednoklávesové zkratky,
                    ale do budoucna je plánována i&nbsp;podpora kombinací kláves.
                </Paragraph>
                <Paragraph>
                    Chceš-li, můžeš si načíst výchozí konfiguraci, která umožňuje základní pohyb pomocí numerické
                    klávesnice. Načtení konfigurace přepíše jen makra na kolidujích klávesách, ostatní makra zůstanou zachována.
                </Paragraph>
                <CommandTable>
                    <thead>
                    <tr>
                        <th>
                            klávesa
                        </th>
                        <th>
                            příkaz
                        </th>
                        <th className="no-border"/>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        commands.map(
                            (command, key) => <CommandRecord key={key} command={command}
                                                             deleteMacro={this.deleteMacro}/>
                        )
                    }
                    <tr>
                        <td className="no-border no-padding">
                            <SelectKeyButton
                                onFocus={this.startDetectingHotkey}
                                onBlur={this.stopDetectingHotkey}
                            >
                                {this.state.detectedHotkey ? this.state.detectedHotkey : (this.state.detectingHotkey ? "..." : "vybrat klávesu")}
                            </SelectKeyButton>
                        </td>
                        <td className="no-border no-padding">
                            <MacroCommandInput onChange={this.onNewCommandInputChange} value={this.state.newCommand}/>
                        </td>
                        <td className="no-border no-padding">{
                            this.state.detectedHotkey && this.state.newCommand ?
                                <AddMacroButton onClick={this.addNewMacro}>přidat makro</AddMacroButton>
                                : null
                        }</td>
                    </tr>
                    </tbody>
                </CommandTable>
                <BottomButtons>
                    <GeneralButton onClick={this.loadDefaultMacros}>načíst výchozí</GeneralButton>
                    <ButtonSpacer/>
                    <DeleteEverythingButton onClick={this.deleteAllMacros}>smazat vše</DeleteEverythingButton>
                </BottomButtons>
            </Dialog>
        );
    }
}

export default observer(Configurator);