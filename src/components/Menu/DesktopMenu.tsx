// @flow

import React from "react";
import styled, {css} from "styled-components";
import MenuItem from "./MenuItem";
import {linkStyle} from "../GlobalStyle";

const StyledMenu = styled.menu`
  padding: 0.5em;
  line-height: 1.5em;
`;

const menuLinkStyle = css`
  ${linkStyle};
  
  &:before {
    content: ">";
    padding-right: 0.2em;
    opacity: 0;
  }
  
  &:hover, &:focus, &:active {
    &:before {
      opacity: 1;
      text-decoration: none;
    }
  }
`;

const RealLink = styled.a`
  ${menuLinkStyle}
`;

const FakeLink = styled.span`
  ${menuLinkStyle}
`;

type Props = {
    items: Array<MenuItem>
}

const DesktopMenu = (props: Props) => {
  return (
      <StyledMenu>
        <ul>
          {props.items.map((item, key) => (
              <li key={key}>
                {item.link
                    ? <RealLink href={item.link} target="_blank" rel="noopener noreferrer">{item.text}</RealLink>
                    : <FakeLink onClick={item.action}>{item.text}</FakeLink>
                }
              </li>
          ))}
        </ul>
      </StyledMenu>
  )
};

export default DesktopMenu;