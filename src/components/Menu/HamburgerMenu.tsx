// @flow

import React, {Component, Fragment} from "react";
import styled, {css} from "styled-components";

import HamburgerButton from "./HamburgerButton";
import {version} from "../../constants";
import MenuItem from "./MenuItem";

import {MdOpenInNew} from "react-icons/md";
import {linkStyle} from "../GlobalStyle";

type Props = {
    items: Array<MenuItem>
};
type State = {
    open: boolean
}

const Menu = styled.menu`
  overflow: hidden;
  
  background: black;
  position: absolute;
  width: 100%;
  left: 0;
  top: 0;

  height: 0;
  opacity: 0;
  
  transition: all 150ms ease-in-out;
  
  display: flex;
  flex-direction: column;
  
  &.open {
    height: 100%;
    opacity: 1;
  }
`;

const FlexboxSpacer = styled.div`
  flex: 1;
`;

const MenuList = styled.ul`    
  padding: 5em 0 0 0;
  
  border-style: none none solid none;
  border-color: white;
  border-width: thin;
`;

const LinkStyle = css`
  color: white;
  text-decoration: none;
  line-height: 1.3em;
  padding: 1.2em 1em 1em 1em;
  display: block;
  border-style: solid none none none;
  border-color: white;
  border-width: thin;
  
  cursor: pointer;
  
  &:hover, &:focus, &:active {
    background: #242424;
  }
`;

const RealLink = styled.a`
  ${LinkStyle}
`;

const FakeLink = styled.span`
  ${LinkStyle}
`;

const OpenInNewIcon = styled(MdOpenInNew)`
    margin-left: 0.3em;
    font-size: 1.2em;
    vertical-align: middle;
    opacity: 0.9;
`;

const VersionInfo = styled.div`
  padding: 0.5em;
  text-align: right;
`;

const Link = styled.a`
  ${linkStyle};
  
  opacity: 0.3;
`;

const NoLink = styled.span`
  opacity: 0.3;
`;

export default class HamburgerMenu extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.state = {open: false};
    }

    toggleMenu = () => {
        this.setState({open: !this.state.open});
    };

    render() {
        return (
            <Fragment>
                <HamburgerButton className={this.state.open ? "open" : null} onClick={this.toggleMenu}/>
                <Menu className={this.state.open ? "open" : ""}>
                    <MenuList>
                        {this.props.items.map((item, key) => (
                            <li key={key}>
                                {item.link
                                    ? <RealLink href={item.link} target="_blank" rel="noopener noreferrer">
                                            {item.text}
                                        <OpenInNewIcon/>
                                      </RealLink>
                                    : <FakeLink onClick={() => {
                                        this.toggleMenu();
                                        if(item.action) {
                                            item.action();
                                        } else {
                                            console.error('FakeLink does not have a valid action!');
                                        }
                                    }}>{item.text}</FakeLink>
                                }
                            </li>
                        ))}
                    </MenuList>
                    <FlexboxSpacer/>
                    <VersionInfo>
                        {process.env.REACT_APP_REPOSITORY_LINK ? (
                            <Link href="https://gitlab.com/janjaromirhorak/prahy-client"
                                  target="_blank"
                                  rel="noopener noreferrer">
                                verze {version}
                            </Link>
                        ) : (
                            <NoLink>
                                verze {version}
                            </NoLink>
                        )}
                    </VersionInfo>
                </Menu>
            </Fragment>
        );
    }
}