import React from "react";
import styled from "styled-components";

const closedStateSpacing = "8px";
const hamburgerWidth = "25px";

const Hamburger = styled.div`
  position: absolute;
  right: 1em;
  top: 1em;
  z-index: 2000;

  background: white;
  width: calc(${hamburgerWidth} + 20px);
  height: calc(${hamburgerWidth} + 20px);
  padding: 12px 0 0 10px;
  display: inline-block;
  cursor: pointer;
  transition-property: opacity, filter;
  transition-duration: 0.15s;
  transition-timing-function: linear;
  font: inherit;
  color: inherit;
  text-transform: none;
  
  border: 0;
  margin: 0;
  overflow: visible;
  
  border-radius: 50%;
`;

const HamburgerBox = styled.span`
  width: 30px;
  height: 20px;
  display: inline-block;
  position: relative;
`;

const HamburgerInner = styled.span`
  display: block;
  top: 50%;
  margin-top: -2px;
  
  transition-duration: 0.075s;
  transition-timing-function: cubic-bezier(0.55, 0.055, 0.675, 0.19);
  
  ${Hamburger}.open &, ${Hamburger}.open &::before, ${Hamburger}.open &::after {
    background-color: #000;
  }
  
  &, &::before, &::after {
    width: ${hamburgerWidth};
    height: 4px;
    background-color: #000;
    position: absolute;
    transition-property: transform;
    transition-duration: 0.15s;
    transition-timing-function: ease;
  }
    
  &::before, &::after {
    content: "";
    display: block;
  }
  
  &::before {
    top: -${closedStateSpacing};
    transition: top 0.075s 0.12s ease, opacity 0.075s ease;
  }
  &::after {
    bottom: -${closedStateSpacing};
    transition: bottom 0.075s 0.12s ease, transform 0.075s cubic-bezier(0.55, 0.055, 0.675, 0.19);
  }
    
  ${Hamburger}.open & {
    transform: rotate(45deg);
    transition-delay: 0.12s;
    transition-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
      
    &::before {
      top: 0;
      opacity: 0;
      transition: top 0.075s ease, opacity 0.075s 0.12s ease;
    }
        
    &::after {
      bottom: 0;
      transform: rotate(-90deg);
      transition: bottom 0.075s ease, transform 0.075s 0.12s cubic-bezier(0.215, 0.61, 0.355, 1);
    }
  }
`;

// todo
const HamburgerButton = (props: any) => (
    <Hamburger {...props}>
        <HamburgerBox>
            <HamburgerInner/>
        </HamburgerBox>
    </Hamburger>
);

export default HamburgerButton;