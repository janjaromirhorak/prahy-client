// @flow

export default class MenuItem {
    text: string;
    link?: string;
    action?: () => void;

    constructor(text: string, link?: string, action?: () => void) {
        this.text = text;
        this.link = link;
        this.action = action;
    }
}