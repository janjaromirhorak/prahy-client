import styled, {css} from "styled-components";
import {fontFamily} from "./GlobalStyle";

export const generalInputStyle = css`
  ${fontFamily};

  font-size: 1em;

  color:white;
  background: black;
  border: 2px solid white;
  padding: 0.4em;
`;

export const GeneralButton = styled.button` 
  ${generalInputStyle};
  
  cursor: pointer;

  text-align: center;
  
  &:hover {
    color: black;
    background: white;
  }
`;

export const NoBorderButton = styled.button`
  ${fontFamily};
  
  padding: 0.8em;
  display: inline-block;
  
  background: black;
  color: white;
  border: none;
  cursor: pointer;
  
  font-size: 1em;
  
  &:hover {
    font-weight: bold;
  }
`;