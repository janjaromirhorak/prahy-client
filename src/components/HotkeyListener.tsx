// @flow

import {Component} from "react";
import {observer} from 'mobx-react';

import type {HotkeyManager} from "../hotkeyManager";

type Props = {
    hotkeyManager: HotkeyManager
};

class HotkeyListener extends Component<Props> {
    componentDidMount(): void {
        // add event listener for hotkeys
        window.addEventListener('keydown', this.props.hotkeyManager.triggerHotkey);
    }

    componentWillUnmount(): void {
        // remove the event listener
        window.removeEventListener('keydown', this.props.hotkeyManager.triggerHotkey);
    }

    render() {
        return null;
    }
}

export default observer(HotkeyListener);