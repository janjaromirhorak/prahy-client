// @flow

import React from "react";
import styled from "styled-components";

import {DesktopMenu} from "./Menu";
import type {MenuItem} from './Menu';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const Spacer = styled.div`
  flex: 1;
`;

const Line = styled.div``;

type Props = {
    topItems: Array<MenuItem>,
    bottomItems: Array<MenuItem>,
    externalItems: Array<MenuItem>
}

const LeftColumn = ({topItems, bottomItems, externalItems}: Props) => (
    <Wrapper>
        <DesktopMenu items={topItems}/>
        <Spacer/>
        <DesktopMenu items={bottomItems}/>
        <Line/>
        <DesktopMenu items={externalItems}/>
    </Wrapper>
);

export default LeftColumn;