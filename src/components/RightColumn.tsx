// @flow

import React from "react";
import styled from "styled-components";

import {version} from "../constants";
import {linkStyle} from "./GlobalStyle";

const VersionInfo = styled.div`
  display: inline-block;
  position: absolute;
  right: 0.5em;
  bottom: 0.5em;
`;

const Link = styled.a`
  ${linkStyle};
  
  opacity: 0.3;
`;

const NoLink = styled.span`
  opacity: 0.3;
`;

const RightColumn = () => {
    return (
        <VersionInfo>
            {process.env.REACT_APP_REPOSITORY_LINK ? (
                <Link href="https://gitlab.com/janjaromirhorak/prahy-client"
                      target="_blank"
                      rel="noopener noreferrer">
                    verze {version}
                </Link>
            ) : (
                <NoLink>
                    verze {version}
                </NoLink>
            )}
        </VersionInfo>
    )
};

export default RightColumn;