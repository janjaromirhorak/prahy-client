// @flow

import React, {Component, PropsWithChildren} from "react";
import styled from 'styled-components';
import Div100vh from 'react-div-100vh'

import CloseButton from "./CloseButton";
import {HotkeyManager} from "../hotkeyManager";
import hotkeyManager from "../hotkeyManager";

const Wrapper = styled(Div100vh)`
  position: fixed;
  left: 0;
  top: 0;

  width: 100%;
  
  text-align: center;
  
  background: rgba(0, 0, 0, 0.5);
  
  overflow: auto;
`;

const Padder = styled.div`
  padding: 5em 2em;
`;

const Container = styled.div`
  position: relative;
  background: black;
  overflow-y: auto;
  display: inline-block;
  
  border: thin solid white;
  
  padding: 2em;
  
  max-width: 100%;
  
  text-align: left;
`;


interface PropsWithoutHotkeyManager extends PropsWithChildren<{}> {
    closeCallback: () => void,
    header?: string
}

interface Props extends PropsWithoutHotkeyManager {
    hotkeyManager: HotkeyManager
}

const DialogCloseButton = styled(CloseButton)`
  position: absolute;
  right: 0.1em;
  top: 0.5em;  
`;

export const DialogHeader = styled.h2`
  padding-bottom: 1.5em;
  font-weight: bold;
  text-transform: uppercase;
  letter-spacing: 0.2em;
`;

class Dialog extends Component<Props> {
    onContainerClick = (event: MouseEvent) => {
        event.stopPropagation();
    };

    componentDidMount() {
        this.props.hotkeyManager.disableHotkeys();
    }

    componentWillUnmount() {
        this.props.hotkeyManager.enableHotkeys();
    }

    render() {
        const {closeCallback, children, ...rest} = this.props;
        return (
            <Wrapper onClick={closeCallback}>
                <Padder>
                    {/* @ts-ignore */}
                    <Container onClick={this.onContainerClick} {...rest}>
                        <DialogCloseButton onClick={closeCallback}/>
                        {this.props.header ? <DialogHeader>{this.props.header}</DialogHeader> : null}
                        {children}
                    </Container>
                </Padder>
            </Wrapper>
        );
    }
}

const DialogWithHotkeyManager = (props: PropsWithoutHotkeyManager) => (
    <Dialog {...props} hotkeyManager={hotkeyManager}/>
);

export default DialogWithHotkeyManager;