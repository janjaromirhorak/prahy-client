// @flow

import React, {MouseEventHandler} from "react";

import styled from "styled-components";
import {NoBorderButton} from "../formElements";
import {fontFamily} from "../GlobalStyle";
import {sgrStyle} from "../../defaultConfig";

interface Props {
    isOn?: boolean,
    onClick?: MouseEventHandler
}

const OnOffSwitchBox = styled(NoBorderButton)<Props>`
    ${fontFamily};

    cursor: pointer;   
    color: ${props => props.isOn ? sgrStyle.brightGreen : "white"}
`;

const OnOffSwitch = ({isOn, ...rest}: Props) => (
    <OnOffSwitchBox isOn={isOn} {...rest}>
        {isOn ? "zapnuto" : "vypnuto"}
    </OnOffSwitchBox>
);

export default OnOffSwitch;