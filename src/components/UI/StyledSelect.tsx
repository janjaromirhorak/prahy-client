// @flow

import React from "react";
import styled from "styled-components";
import {MdArrowDropDown} from "react-icons/md";

import {fontFamily} from "../GlobalStyle";

const StyledSelect = styled.select`
  -webkit-appearance:none; 
  -moz-appearance:none; 
  appearance:none;  

  ${fontFamily};
  
  font-size: 1em;
  
  color: white;
  background: black;
  border: 2px solid white;
  padding: 0.4em;
  
  width: 10em;
  
  cursor: pointer;
`;

const Arrow = styled(MdArrowDropDown)`
  font-size: 2em;
  position: absolute;
  right: 0;
  top: 0.05em;
  pointer-events: none;
`;

const StyledSelectWrapper = styled.div`
  display: inline-block;
  position:relative;
`;

const CustomSelect = (props: any) => (
    <StyledSelectWrapper>
        <StyledSelect {...props}/>
        <Arrow/>
    </StyledSelectWrapper>
);

export default CustomSelect;