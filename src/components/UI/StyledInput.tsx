// @flow

import styled from "styled-components";
import {fontFamily} from "../GlobalStyle";

export default styled.input`
  ${fontFamily};

  font-size: 1em;

  color:white;
  background: black;
  border: 2px solid white;
  padding: 0.4em;
  
  width: 100%;
`;