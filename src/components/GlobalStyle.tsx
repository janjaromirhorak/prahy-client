// @flow

import {createGlobalStyle, css} from 'styled-components'
import reset from 'styled-reset'

const highlightColor = "#CCCCCC";

export const fontFamily = css`
  font-family: 'Courier Prime', monospace;
`;

export const linkStyle = css`
  color: white;
  opacity: 0.5;
  text-decoration: none;
  cursor: pointer;
  
  &:hover, &:focus, &:active {
    opacity: 1;
  }
`;

export default createGlobalStyle`
  ${reset}
  
  * {
    box-sizing: border-box;
  }
  
  body {
    color: white;
    background: black;
    ${fontFamily};
  }
  
  strong {
    font-weight: 700;
  }
  
  ::selection {
    color:black;
    background: ${highlightColor};
  }

  ::-moz-selection {
    color: black;
    background: ${highlightColor};
  }
  
  // tooltip styling
  .react-tooltip-lite {
    background: white;
    color: black;
    
    @media(min-width: 50em) {
      max-width: 50em !important;
    }
  }
`;