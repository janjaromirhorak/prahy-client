// @flow

import {action, observable} from "mobx";

type HotkeyAction = () => void;

type HotkeyBind = {
    description?: string,
    action?: HotkeyAction,
    terminalHotkeyCommand?: string
}

const blankBind: HotkeyBind = {
    description: undefined,
    action: undefined,
    terminalHotkeyCommand: undefined
};

export class HotkeyManager {
    hotkeys: any;
    hotkeysEnabled: any;
    terminalMacroCallback?: (event: KeyboardEvent, str: string) => void;

    constructor() {
        this.hotkeys = observable({});
        this.hotkeysEnabled = observable.box(true);
        this.terminalMacroCallback = undefined;

        this.loadHotkeysFromLocalStorage();
    }

    filterTerminalHotkeys(hotkeys: any) {
        let terminalHotkeys: any = {}; // todo

        const keys = Object.keys(hotkeys);
        if (keys) {
            for (const key of keys) {
                if (hotkeys[key] && hotkeys[key].terminalHotkeyCommand) {
                    terminalHotkeys[key] = hotkeys[key].terminalHotkeyCommand;
                }
            }
        }

        return terminalHotkeys;
    }

    saveTerminalHotkeysToLocalStorage(terminalHotkeys: any) {
        window.localStorage.setItem('macros', JSON.stringify(terminalHotkeys));
        window.localStorage.setItem('macros_version', "1");
    };

    getTerminalHotkeysFromLocalStorage() {
        let terminalHotkeysEncoded = window.localStorage.getItem('macros');
        if (terminalHotkeysEncoded) {
            try {
                let terminalHotkeys = JSON.parse(terminalHotkeysEncoded);

                if (terminalHotkeys) {
                    return terminalHotkeys;
                }
            } catch (e) {
                window.localStorage.removeItem('macros');
            }
        }
        return {};
    };

    loadHotkeysFromLocalStorage = action(() => {
        const hotkeys = this.getTerminalHotkeysFromLocalStorage();
        const keys = Object.keys(hotkeys);
        for (const key of keys) {
            this.registerTerminalHotkey(key, hotkeys[key], false);
        }
    });

    enableHotkeys = action(() => {
        this.hotkeysEnabled.set(true);
    });

    disableHotkeys = action(() => {
        this.hotkeysEnabled.set(false);
    });

    registerHotkeyAction = action((key: string, callback: (event: KeyboardEvent) => void) => {
        if (!this.hotkeys[key]) {
            this.hotkeys[key] = {...blankBind};
        }
        this.hotkeys[key].action = callback;
    });

    unregisterHotkey = action((key: string) => {
        this.hotkeys[key] = undefined;

        this.saveTerminalHotkeysToLocalStorage(this.filterTerminalHotkeys(this.hotkeys));
    });

    getHotkeyDescription = action((key: string) => {
        if (this.hotkeys[key]) {
            return this.hotkeys[key].description;
        }

        return undefined;
    });

    registerTerminalMacroCallback = action((callback: (e: KeyboardEvent, str: string)=>void) => {
        this.terminalMacroCallback = callback;
    });

    unregisterTerminalMacroCallback = action(() => {
        this.terminalMacroCallback = undefined;
    });

    registerTerminalHotkey = action((key: string, command: string, saveToLocalStorage: boolean = true) => {
        this.hotkeys[key] = {...blankBind};
        this.hotkeys[key].terminalHotkeyCommand = command;

        if (saveToLocalStorage) {
            this.saveTerminalHotkeysToLocalStorage(this.filterTerminalHotkeys(this.hotkeys));
        }
    });

    triggerHotkey = (event: KeyboardEvent) => {
        if (this.hotkeysEnabled.get()) {
            // if the key has some value
            if (this.hotkeys[event.code]) {
                const {action, terminalHotkeyCommand} = this.hotkeys[event.code];

                if (action) {
                    this.hotkeys[event.code].action(event);
                } else if (terminalHotkeyCommand) {
                    // check first if the callback exists
                    if (this.terminalMacroCallback) {
                        this.terminalMacroCallback(event, this.hotkeys[event.code].terminalHotkeyCommand);
                    }
                }
            }
        }
    }
}

let hotkeyManager = new HotkeyManager();

export default hotkeyManager;