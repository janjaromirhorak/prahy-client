import React, {Fragment, useEffect, useMemo} from 'react';

// intersection observer polyfill
import 'intersection-observer';

// TextEncoder / TextDecoder polyfill
import 'fast-text-encoding';

import {GlobalStyle, TitleManager, HotkeyListener, PageLayout} from "./components";
import hotkeyManager from "./hotkeyManager";
import appState from "./appState";

const App = () => {
    const redirect = useMemo(() => {
        return (window.location.pathname && window.location.pathname !== "/");
    }, []);

    // if we are on a subpage, redirect to /
    useEffect(() => {
        if (redirect) {
            window.location.replace(`${window.location.origin}/`);
        }
    }, [redirect]);

    return (
        <Fragment>
            <TitleManager appState={appState}/>
            <GlobalStyle/>
            <HotkeyListener hotkeyManager={hotkeyManager}/>
            {redirect ? null : (
                <PageLayout appState={appState}/>
            )}
        </Fragment>
    );
};

export default App;
