const version: string =  process.env.REACT_APP_VERSION ? process.env.REACT_APP_VERSION : "?.?.?";

export {
    version
}