const getEnumeratedWords = (words: Array<string> = []): string => {
    if (words.length === 0) {
        return "";
    } else if (words.length === 1) {
        return words.pop() ?? "";
    } else if (words.length === 2) {
        const [a, b] = words;
        return `${a} a ${b}`;
    } else {
        const [a, b, ...rest] = words;
        return getEnumeratedWords([`${a}, ${b}`, ...rest]);
    }
};

const checkBrowser = (addErrorMessage: (string: string) => void): boolean => {
    let unsupportedNames = [];

    // TextDecoder
    const supportsTextDecoder = typeof (TextDecoder) !== 'undefined';

    if (!supportsTextDecoder) {
        unsupportedNames.push("TextDecoder");
    }

    const supportsTextEncoder = typeof (TextEncoder) !== 'undefined';

    if (!supportsTextEncoder) {
        unsupportedNames.push("TextEncoder");
    }

    // WebSocket
    const supportsWebSockets = typeof (WebSocket) !== 'undefined';

    if (!supportsWebSockets) {
        unsupportedNames.push("WebSockets");
    }

    // Uint8Array
    const supportsUint8Array = typeof (Uint8Array) !== 'undefined';

    if (!supportsUint8Array) {
        unsupportedNames.push("Uint8Array");
    }

    // Blob
    const supportsBlob = typeof (Blob) !== "undefined";

    if (!supportsBlob) {
        unsupportedNames.push("Blob");
    }

    // LocalStorage
    const supportsLocalStorage = !! typeof (window.localStorage);

    if(!supportsLocalStorage) {
        unsupportedNames.push("LocalStorage");
    }

    const supported = supportsTextDecoder && supportsTextEncoder && supportsWebSockets && supportsUint8Array && supportsBlob && supportsLocalStorage;

    if (!supported) {
        addErrorMessage(`Tvůj prohlížeč bohužel neumí vše, co je potřeba pro běh webového klienta. Nezná totiž ${getEnumeratedWords(unsupportedNames)}.`);
    }

    return supported;
};

export default checkBrowser;