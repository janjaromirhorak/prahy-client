// @flow

const createMailtoUrl = (email: string, subject?: string, body?: string): string => {
    let link = `mailto:${email}`;
    if (subject) {
        link += `?subject=${encodeURI(subject)}`
    }
    if (body) {
        if (subject) {
            link += "&";
        } else {
            link += "?";
        }
        link += `body=${encodeURI(body)}`;
    }

    return link;
};

export default createMailtoUrl;