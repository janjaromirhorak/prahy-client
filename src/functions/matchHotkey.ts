// @flow

const matchHotkey = (code: string) => {
    const hotkeyCode = code;
    return (
        (code: string) => {
            return hotkeyCode === code;
        }
    );
};

export default matchHotkey;