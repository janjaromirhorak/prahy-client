// @flow

import {version} from "../constants";

const createBugReportTemplate = () => (
    `Verze klienta: ${version}
     Prohlížeč: ${window.navigator.userAgent}
     
     Popis chyby: `
);

export default createBugReportTemplate;